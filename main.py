"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2016, Twitter: @VFisa'"


from keboola import docker
import requests
import base64
import json
import csv
import datetime
import re


# initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

# access the supplied values
COMMUNITY = cfg.get_parameters()['COMMUNITY']
#RESOURCE = cfg.get_parameters()['RESOURCE']
USERNAME = cfg.get_parameters()['USERNAME']
PASSWORD = cfg.get_parameters()['PASSWORD']


wordDic = {
    '_SPACE': ' ',
    '_COLON': ':',
    '_DOLAR': '$'
    }


def special_chars(text, wordDic):
    """
    Validate column field names so they are VC friendly (bring back obscure names).
    This is the result of VC, using spaces and non-standard characters in the variables,
    since they use it as a value, not as a column name.
    There are limitations of column names in KBC and MySQL, so this is putting back obscurity for VC
    # by Python Fan 'bvdet'
    # probably not really pythonic way
    """
    rc = re.compile('|'.join(map(re.escape, wordDic)))
    def translate(match):
        return wordDic[match.group(0)]
        
    return rc.sub(translate, text)


def csv_to_json(file = '/data/in/tables/data.csv'):
    """
    Convert passed csv to json specific for VC API structure
    """
    
    f = open(file,'rU')
    reader = csv.DictReader(f)
    
    outR = json.dumps( [ row for row in reader ] , indent=4)
    out = json.loads(str(outR))
    
    variables = []
    today = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')

    for a in out:
        ## get main id = email
        memid = str(a[u'email'])

        ## mutate data and remove email from the fields to be written
        data = a
        if u'email' in data:
            del data[u'email']
        ## list of variables table
        keys = data.keys()

        ## assemble the whole thingy
        for b in keys:
            ## be avare of stripping out UNICODE
            variableName = special_chars(str(b), wordDic)
            snippet = {
        	    "memberVariableName": variableName,
        	    "memberDataSetType": "Live",
        	    "memberEmail": memid,
        	    "memberVariableValue": str(a[b]),
        	    "effectiveDate": today
                }
            variables.append(snippet)
    
    print "data converted to json."
    return variables


def post_data(auth_code, data):
    """Create new variables"""
    
    URL = str("https://api.visioncritical.com/v1/applications/"+COMMUNITY+"/membervalues")
    print "calling: "+str(URL)
    
    HEADERS = {
        "authorization": auth_code
        ,"cache-control": "no-cache"
        ,"X-WebApi-Return-Resource":"true"
        }
    
    #header removal
    if len(data)>1:
        data = data[1:] 
    else:
        pass
    print ("preparing requests...")

    returned = []

    # request
    for a in data:
        PAYLOAD = a
        print "Adding: "+str(a[u'memberVariableName'])+" - "+str(a[u'memberEmail']) # left u' just for the case
        
        # Call itself
        print ("sending request... response:")
        response = requests.post(URL, headers = HEADERS, data = json.dumps(PAYLOAD))
        #response = requests.request("POST", URL, json = [{"dataType": "text", "type": "open", "name": "testvariable"}], headers = HEADERS)
        
        ## KBC logger should be done better, another area for further improvements
        print ("Code: "+str(response.status_code))
        #print response.text
        returned.append(response.text)
        #print response.headers
    
    print "done."
    return returned


## AUTH PART
encoded = base64.b64encode(str(USERNAME + ":" + PASSWORD))
auth_code = str("Basic " + encoded)

## DATA PREP
#data_file = open('/data/in/tables/data.csv', 'w') #KBC
jsonU = csv_to_json()

## EXECUTE
log = post_data(auth_code, jsonU)
print ("-----")
#print log
